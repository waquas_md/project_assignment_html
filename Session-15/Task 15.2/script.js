function getRandomIndex() {

    var randomIndex = Math.floor(Math.random() * 8);
    return randomIndex;
};

function showCorrect() {
    alert('correct');
}

function showTryAgain() {
    alert('Oops..Incorrect...Try Again');
}

function changeQuestion() {
    var questions = ['Click on a circle shaped object', 'Click on a rectangle shaped object', 'Click on a triangle shaped object', 'Click on a squre shaped object', 'Click on a cloud', 'Click on a flower plant', 'Click on a tree', 'Click on a crow',];
    var questionNumber = getRandomIndex();
    console.log(questionNumber);
    $('#question-holder').text(questions[questionNumber]);
    $('#question-holder').attr('que-index', questionNumber);

};

function verifyAnswerAndUpdateTotal(element) {
    var questionIndex = $("#question-holder").attr('que-index');
    if (questionIndex == 0) {
        if ($(element).attr('id') == 'circle') {
            showCorrect();
        }
        else {
            showTryAgain();
        }
    }

    if (questionIndex == 1) {
        if ($(element).attr('id') == 'rectangle') {
            showCorrect();
        }
        else {
            showTryAgain();
        }
    }

    if (questionIndex == 2) {
        if ($(element).attr('id') == 'triangle') {
            showCorrect();
        }
        else {
            showTryAgain();
        }
    }

    if (questionIndex == 3) {
        if ($(element).attr('id') == 'square') {
            showCorrect();
        }
        else {
            showTryAgain();
        }
    }

    if (questionIndex == 4) {
        if ($(element).attr('id') == 'cloud') {
            showCorrect();
        }
        else {
            showTryAgain();
        }
    }

    if (questionIndex == 5) {
        if ($(element).attr('id') == 'flower') {
            showCorrect();
        }
        else {
            showTryAgain();
        }
    }

    if (questionIndex == 6) {
        if ($(element).attr('id') == 'tree') {
            showCorrect();
        }
        else {
            showTryAgain();
        }
    }

    if (questionIndex == 7) {
        if ($(element).attr('id') == 'crow') {
            showCorrect();
        }
        else {
            showTryAgain();
        }
    }

    changeQuestion();

};
$(document).ready(function () {

    changeQuestion();
    $('.answer').click(function () {
        verifyAnswerAndUpdateTotal(this);
    });
});