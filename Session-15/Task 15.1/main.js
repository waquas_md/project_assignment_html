function allImages {
    var imageSet = $('.image-class');
}
function mailForm {
    var mail = $('.mail-class');
}
function classCollection {
    var collection = $('#collection');
}
function imageHeading {
    var imageHeadingID = $('.#imageHeadingID');
}
function navigationMenu {
    var navigID = $('#navigID');
}
function gdiLogo {
    var gdilogo = $('.gdilogo-class');
}
function mediaLogo {
    var media = $('.media-class');
}
function redDots {
    var redDot = $('.redDot-class');
}

$(document).ready(function() {
    allImages();
    mailForm();
    classCollection();
    imageHeading();
    navigationMenu();
    gdiLogo();
    mediaLogo();
    redDots();
});