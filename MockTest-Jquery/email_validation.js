function validation() {
    var result=true;
    var i=document.getElementsByName("email")[0].value;
    var atIndex=i.indexOf('@');
    var dotIndex=i.lastIndexOf('.');
    if(atIndex<1 || dotIndex>=i.length-2 || dotIndex-atIndex<3) {
        alert("insert correct pattern of email..");
        result=false;
    }
    return(result);
}