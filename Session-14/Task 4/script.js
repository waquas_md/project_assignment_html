var arrayObj=[
    {
        "filename":"flower",
        "type": "jpg"
    },
    {
        "filename":"family-video-clip",
        "type": "mp4"
    },
    {
        "filename":"phone-ringtone",
        "type": "mp3"
    },
    {
        "filename":"javascript-exercises",
        "type": "txt"
    },
    {
        "filename":"leaning-html-basics",
        "type": "rtf"
    },
    {
        "filename":"friend-video-clip",
        "type": "mp4"
    },
    {
        "filename":"resume",
        "type": "docx"
    },
    {
        "filename":"student-report",
        "type": "csv"
    },
    {
        "filename":"sms-ringtone",
        "type": "mp3"
    },
    {
        "filename":"html-basics",
        "type": "pdf"
    },
    {
        "filename":"dubsmash",
        "type": "mp4"
    },
    {
        "filename":"screen-shot",
        "type": "png"
    },
    {
        "filename":"faculty-report",
        "type": "xlsx"
    },
    {
        "filename":"puppy",
        "type": "svg"
    }
];
var pngFile=[], pdfFile=[], rtfFile=[], txtFile=[], svgFile=[], xlsxFile=[];
var docxFile=[], mp4File=[], jpgFile=[], mp3File=[];
var m=0;var n=0;var o=0;var p=0;
var counter=arrayObj.length;
var a=0;var b=0;var c=0;var d=0;var e=0;var f=0;
for(i=0;i<counter;i++) {
    if(arrayObj[i].type=="mp3") {
        
        mp3File[a++]=arrayObj[i].filename;
    }
    else if(arrayObj[i].type=="jpg") {
        
        jpgFile[b++]=arrayObj[i].filename;
    }
    else if(arrayObj[i].type=="mp4") {
        
        mp4File[c++]=arrayObj[i].filename;
    }
    else if(arrayObj[i].type=="docx") {
        
        docxFile[d++]=arrayObj[i].filename;
    }
    else if(arrayObj[i].type=="xlsx") {
        
        xlsxFile[e++]=arrayObj[i].filename;
    }
    else if(arrayObj[i].type=="svg") {
        
        svgFile[f++]=arrayObj[i].filename;
    }
    else if(arrayObj[i].type=="txt") {
        
        txtFile[m++]=arrayObj[i].filename;
    }
    else if(arrayObj[i].type=="rtf") {
        
        rtfFile[n++]=arrayObj[i].filename;
    }
    else if(arrayObj[i].type=="pdf") {
        
        pdfFile[o++]=arrayObj[i].filename;
    }
    else if(arrayObj[i].type=="png") {
        
        pngFile[p++]=arrayObj[i].filename; 
    }
}
document.write("<br><br>"+"JPG File :" + jpgFile+'<br><br>');
document.write("MP3 File :" + mp3File+'<br><br>');
document.write("MP4 File :" + mp4File+'<br><br>');
document.write("DOCX File :" + docxFile+'<br><br>');
document.write("TXT File :" + txtFile+'<br><br>');
document.write("RTF File :" + rtfFile+'<br><br>');
document.write("PDF File :" + pdfFile+'<br><br>');
document.write("PNG File :" + pngFile+'<br><br>');
document.write("XLSX File :" + xlsxFile+'<br><br>');
document.write("SVG File :" + svgFile+'<br><br>');