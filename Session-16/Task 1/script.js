const startingMinutes=1;
let time= startingMinutes * 60;
const countdownEl=document.getElementById('countDown');
var x=setInterval(updateCountDown, 1000);
function updateCountDown() {
    if(time>=0) {
    const minutes= Math.floor( time / 60);
    let seconds=time % 60;
    seconds= seconds<10 ? '0'+seconds : seconds;
    countdownEl.innerHTML= minutes+':'+seconds;
    time--;
    }
    else {
        clearInterval(x);
        countdownEl.innerHTML="Game...Over";
    }   
}
function displayRandomCircles()
{
    var circles = ['circle1','circle2','circle3','circle4','circle5'];
    $.each(circles, function(index,value){
        var id = value;
        //console.log(id);
        $('#'+id).hide();
    });
    var circleId = randomIntFromInterval();
    $('#circle'+circleId).show();
}
function keepTimer(){
}
function updateScore(element){
    var currentScore = $('#score-keeper').val();
    if(currentScore==''||currentScore==undefined)
    {
        currentScore = 0;
    }
    var circleClicked = $(element).attr('class');
    if(circleClicked=='RedCircle')
    {
        var newScore = parseInt(currentScore) - parseInt(2);
        $('#score-keeper').val(newScore);
    }
    if(circleClicked=='VioletCircle')
    {
        var newScore = parseInt(currentScore) - parseInt(1);
        $('#score-keeper').val(newScore);
    }
    if(circleClicked=='GreenCircle')
    {
        var newScore = parseInt(currentScore) + parseInt(3);
        $('#score-keeper').val(newScore);
    }
    if(circleClicked=='YellowCircle')
    {
        var newScore = parseInt(currentScore) + parseInt(2);
        $('#score-keeper').val(newScore);
    }
    if(circleClicked=='BlueCircle')
    {
        var newScore = parseInt(currentScore) + parseInt(1);
        $('#score-keeper').val(newScore);
    }
}
function randomIntFromInterval() { 
    // min and max included 
    var min = 1;
    var max = 5;
    return Math.floor(Math.random() * (max - min + 1) + min);
}
$(document).ready(function(){
    var circles = ['circle1','circle2','circle3','circle4','circle5'];
    $.each(circles, function(index,value){
        var id = value;
        console.log(id);
        $('#'+id).hide();
    });
    window.setInterval(function(){
        displayRandomCircles();
    }, 1000);

    $( "div[id^='circle']" ).click(function(){
        updateScore(this);
    });
});